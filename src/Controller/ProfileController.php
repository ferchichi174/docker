<?php

namespace App\Controller;

use App\Repository\ChefRepository;
use App\Repository\UserRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/admin/profile', name: 'app_profile')]
    public function index(): Response
    {
       $user = $this->getUser();
        return $this->render('profile/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/admin/profile/update", name="app_profile_update", methods={"POST"})
     */
    public function update(Request $request,ChefRepository $ChefRepository): Response
    {
        $data = $request->request->all();
        $chef = $this->getUser();


        $chef->setEmail($data["email"]);
        $chef->setFirstname($data["firstname"]);
        $chef->setlastname($data["lastname"]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        // Rediriger vers une page de confirmation ou afficher un message de succès
        $this->addFlash('success', 'Profil mis à jour avec succès.');

        return $this->redirectToRoute('app_profile');
    }
}
