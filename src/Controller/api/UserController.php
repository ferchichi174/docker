<?php

namespace App\Controller\api;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//#[IsGranted("ROLE_USER")]
class UserController extends AbstractApiController
{

    #[Route('/api/user', name: 'user', methods: ['GET'])]

    public function getAll(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();

        // Customize the JSON response
        $responseData = [];

        foreach ($users as $user) {
            $responseData[] = [
                'id' => $user->getId(),
                'name' => $user->getUsername(),
                'email' => $user->getEmail(),
                'password' => $user->getPassword(),
                'username' => $user->getUsername(),
                'created_at' => $user->getUserCreatetime(),
                'ROLE' => $user->getRoles(),
                
                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }

    #[Route('/api/user/{id}', name: 'showuser', methods: ['GET'])]
    public function showUser($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->respond('Utilisateur non trouvé', Response::HTTP_NOT_FOUND);
        }
        $responseData[] = [
            'id' => $user->getId(),
            'name' => $user->getUsername(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'username' => $user->getUsername(),
            'created_at' => $user->getUserCreatetime(),
            'ROLE' => $user->getRoles(),
//            'ROLE1' => $user->getROLEROLE()->getROLENAME(),
            // Add more fields as needed
        ];

        // Supprimer l'utilisateur
        return $this->respond($responseData, Response::HTTP_OK);
    }


    #[Route('/api/user/add', name: 'useradd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(UserType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var User $customer */
        $user = $form->getData();
        $plainPassword = $user->getPassword();
        $hashedPassword = password_hash($plainPassword, PASSWORD_DEFAULT);
        $user->setPassword($hashedPassword);
        $user->setUserCreatetime(new \DateTime());

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($user);
    }

    #[Route('/api/user/{id}', name: 'deleteuser', methods: ['DELETE'])]
    public function deleteUser($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->respond('Utilisateur non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->respond('Utilisateur supprimé', Response::HTTP_OK);
    }

    #[Route('/api/user/{id}', name: 'updateuser', methods: ["PUT", "PATCH"])]
    public function updateUser(Request $request, $id)
    {
        // Récupérer l'utilisateur à mettre à jour depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->respond('Utilisateur non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Récupérer les données de la requête
        $data = json_decode($request->getContent(), true);

        // Mettre à jour les propriétés de l'utilisateur
        $user->setUSERNAME($data['username']);
        $user->setEmail($data['email']);
        $user->setPassword(password_hash($data['password'], PASSWORD_DEFAULT));

        // Enregistrer les modifications dans la base de données
        $entityManager->flush();

        return $this->respond('Utilisateur mis à jour', Response::HTTP_OK);
    }

}
