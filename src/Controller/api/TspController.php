<?php

namespace App\Controller\api;

use App\Entity\TSP;
use App\Form\TspType;
use App\Repository\TSPRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TspController extends AbstractApiController
{
    #[Route('api/tsp', name: 'app_tsp', methods: ['GET'])]
    public function getAll(TSPRepository $tspepository): Response
    {
        $tsps= $tspepository->findAll();


        // Customize the JSON response
        $responseData = [];

        foreach ($tsps as $tsp) {
            $responseData[] = [
                'id' => $tsp->getId(),
                'TSP_REF' => $tsp->getTSPREF(),
                'TSP_MARQUE' => $tsp->getTSPMARQUE(),
                'TSP_DTENTRY' => $tsp->getTSPDTENTRY(),
//                'user' => $tsp->getUsers()->g,


//                'ABONNE' => $abonne->getABONNE()->getId(),




                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }

    #[Route('/api/tsp/add', name: 'tspadd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(TspType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var TSP $customer */
        $tsp = $form->getData();


        $this->getDoctrine()->getManager()->persist($tsp);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($tsp);
    }

    #[Route('/api/tsp/{id}', name: 'deletetsp', methods: ['DELETE'])]
    public function deletetsp($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $tsp = $entityManager->getRepository(TSP::class)->find($id);

        if (!$tsp) {
            return $this->respond('tsp non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($tsp);
        $entityManager->flush();
        return $this->respond('tsp supprimé', Response::HTTP_OK);
    }

    #[Route('/api/tsp/{id}', name: 'showabonne', methods: ['GET'])]
    public function showTsp($id)
    {

        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $tsp = $entityManager->getRepository(TSP::class)->find($id);

        if (!$tsp) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        $responseData[] = [
            'id' => $tsp->getId(),
            'TSP_REF' => $tsp->getTSPREF(),
            'TSP_MARQUE' => $tsp->getTSPMARQUE(),
            'TSP_DTENTRY' => $tsp->getTSPDTENTRY(),
            'Users' => $tsp->getUsers(),





            // Add more fields as needed
        ];

        return $this->respond($responseData, Response::HTTP_OK);
    }

    #[Route('/api/tsp/{id}', name: 'updateabonnee', methods: ["PUT", "PATCH"])]
    public function updateTsp(Request $request, $id)
    {
        // Récupérer l'utilisateur à mettre à jour depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $tsp = $entityManager->getRepository(TSP::class)->find($id);

        if (!$tsp) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Récupérer les données de la requête
        $data = json_decode($request->getContent(), true);

        // Mettre à jour les propriétés de l'utilisateur
        $tsp->setTSPREF($data['TSP_REF']);
        $tsp->setTSPMARQUE($data['TSP_MARQUE']);;
        $tsp->setTSPDTENTRY($data['TSP_DTENTRY']);

        // Enregistrer les modifications dans la base de données
        $entityManager->flush();

        return $this->respond('tsp mis à jour', Response::HTTP_OK);
    }


}
