<?php

namespace App\Controller\api;

use App\Entity\LIGNEABONNE;
use App\Form\LgAbonneType;
use App\Repository\LIGNEABONNERepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LgAbonneeController extends AbstractApiController
{
    #[Route('api/lgabonnee', name: 'app_lg_abonnee')]
    public function getAll(LIGNEABONNERepository $lgABONNERepository): Response
    {
        $lgabonnes= $lgABONNERepository->findAll();

        // Customize the JSON response
        $responseData = [];

        foreach ($lgabonnes as $abonne) {
            $responseData[] = [
                'id' => $abonne->getId(),
                'LGABONNE_TARIF' => $abonne->getLGABONNETARIF(),
                'CPTEUR_NUM' => $abonne->getCPTEURNUM(),
                'RC_CCPTEUR_CODE' => $abonne->getRCCCPTEURCODE(),
                'amperage' => $abonne->getAmperage(),
                'coeff' => $abonne->getCoeff(),
                'usage' => $abonne->getLusage(),
                'LIGNE_ABONNE_ETATCOMPTEUR' => $abonne->getLIGNEABONNEETATCOMPTEUR(),
                'LIGNE_ABONNE_ANCIENINDEX' => $abonne->getLIGNEABONNEANCIENINDEX(),
                'LIGNE_ABONNE_NBRREMOIS' => $abonne->getLIGNEABONNENBRREMOIS(),
//                'ABONNE' => $abonne->getABONNE()->getId(),




                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }
    #[Route('/api/lgabonnee/add', name: 'lgabonneeadd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(LgAbonneType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var LIGNEABONNE $customer */
        $LgAbonne = $form->getData();


        $this->getDoctrine()->getManager()->persist($LgAbonne);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($LgAbonne);
    }

    #[Route('/api/lgabonne/{id}', name: 'deletelgabonne', methods: ['DELETE'])]
    public function deleteUser($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $abonnee = $entityManager->getRepository(LIGNEABONNE::class)->find($id);

        if (!$abonnee) {
            return $this->respond('lgabonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($abonnee);
        $entityManager->flush();
        return $this->respond('lgabonnee supprimé', Response::HTTP_OK);
    }
    #[Route('/api/lgabonne/{id}', name: 'showabonne', methods: ['GET'])]
    public function showUser($id)
    {

        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $lgabonnee = $entityManager->getRepository(LIGNEABONNE::class)->find($id);

        if (!$lgabonnee) {
            return $this->respond('Lgabonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        $responseData[] = [
            'id' => $lgabonnee->getId(),
            'LGABONNE_TARIF' => $lgabonnee->getLGABONNETARIF(),
            'CPTEUR_NUM' => $lgabonnee->getCPTEURNUM(),
            'RC_CCPTEUR_CODE' => $lgabonnee->getRCCCPTEURCODE(),
            'amperage' => $lgabonnee->getAmperage(),
            'coeff' => $lgabonnee->getCoeff(),
            'usage' => $lgabonnee->getLusage(),
            'LIGNE_ABONNE_ETATCOMPTEUR' => $lgabonnee->getLIGNEABONNEETATCOMPTEUR(),
            'LIGNE_ABONNE_ANCIENINDEX' => $lgabonnee->getLIGNEABONNEANCIENINDEX(),
            'LIGNE_ABONNE_NBRREMOIS' => $lgabonnee->getLIGNEABONNENBRREMOIS(),



            // Add more fields as needed
        ];

        return $this->respond($responseData, Response::HTTP_OK);
    }

    #[Route('/api/lgabonne/{id}', name: 'updatelgabonnee', methods: ["PUT", "PATCH"])]
    public function updatelgabonne(Request $request, $id)
    {
        // Récupérer l'utilisateur à mettre à jour depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $abonnee = $entityManager->getRepository(LIGNEABONNE::class)->find($id);

        if (!$abonnee) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Récupérer les données de la requête
        $data = json_decode($request->getContent(), true);

        // Mettre à jour les propriétés de l'utilisateur
        $abonnee->setLGABONNETARIF($data['LGABONNE_TARIF']);
        $abonnee->setCPTEURNUM($data['CPTEUR_NUM']);
        $abonnee->setRCCCPTEURCODE($data['RC_CCPETUR_CODE']);
        $abonnee->setAmperage($data['amperage']);
        $abonnee->setCoeff($data['coeff']);
        $abonnee->setLUSAGE($data['lusage']);
        $abonnee->setLIGNEABONNEETATCOMPTEUR($data['LIGNE_ABONNE_ETATCOMPTEUR']);
        $abonnee->setLIGNEABONNEANCIENINDEX($data['LIGNE_ABONNE_ANCIENINDEX']);
        $abonnee->setLIGNEABONNENBRREMOIS($data['LIGNE_ABONNE_NBRREMOIS']);

        // Enregistrer les modifications dans la base de données
        $entityManager->flush();

        return $this->respond('lgabonnée mis à jour', Response::HTTP_OK);
    }

}
