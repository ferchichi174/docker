<?php

namespace App\Controller\api;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    private $passwordEncoder;
    private $tokenManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, JWTTokenManagerInterface $tokenManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenManager = $tokenManager;
    }

    public function login(Request $request)
    {
        // Récupérer les informations de connexion de l'utilisateur depuis la requête
        $username = $request->request->get('email');
        $password = $request->request->get('password');
        $tsp = $request->request->get('tsp');

        // Vérifier les informations d'identification de l'utilisateur
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $username]);
        if ($user->getTsp()->getTSPREF() != $tsp ){
            throw $this->createNotFoundException("TSP non autorisé");
        }

        if (!$user || !$this->passwordEncoder->isPasswordValid($user, $password)) {
            throw $this->createNotFoundException('Invalid credentials.');
        }

        // Générer le jeton JWT
        $token = $this->tokenManager->create($user);

        // Retourner le jeton JWT en tant que réponse
        return $this->json(['token' => $token]);
    }
}
