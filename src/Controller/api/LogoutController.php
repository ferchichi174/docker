<?php

namespace App\Controller\api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class LogoutController extends AbstractApiController
{

    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function logout(Request $request): Response
    {
        // Retrieve the user's token
        $token = $this->tokenStorage->getToken();

        if ($token !== null) {
            // Set the authenticated flag to false
            $token->setAuthenticated(false);
        }

        // Clear the token from the TokenStorage
        $this->tokenStorage->setToken(null);

        // Respond with an empty response or a successful logout message
        return $this->respond('Utilisateur deconnecté', Response::HTTP_OK);
    }

}
