<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrgTournController extends AbstractController
{
    #[Route('/admin/organisation/tourne', name: 'app_org_tourn')]
    public function index(): Response
    {
        return $this->render('org_tourn_contoller/index.html.twig', [
            'controller_name' => 'OrgTournController',
        ]);
    }
}
