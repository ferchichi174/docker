<?php

namespace App\Form;

use App\Entity\RELEVE;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RELEVEType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('PQ_DATTRAIT',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('DIST_CODE')
            ->add('LOC_CODE')
            ->add('PQ_NUM')
            ->add('ZONE_CODE')
            ->add('CPTRLV_REF')
            ->add('CPTRLV_NUMCPTEUR')
            ->add('CPTRLV_TARIFCAPTAGE')
            ->add('CPTRLV_PUISSCPTEUR')
            ->add('CPTRLV_AINDEX')
            ->add('CPTRLV_NINDEX')
            ->add('CPTRLV_NBRMOIS')
            ->add('CPTRLLV_COEFF')
            ->add('CPTRLV_CONSMOY')
            ->add('OBSERV_CODE')
            ->add('ERLV_CODE')
            ->add('ERR_CODE')
            ->add('CPTRLV_VALIDRELV')
            ->add('CPTRLV_VALIDCHEF')
            ->add('CPTRLV_DATMAJ',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTRLV_UF_CODE')
            ->add('CPTRLV_IDUTILISATEUR')
            ->add('EMPCPTR_ACCES')
            ->add('EMPCPTR_CODE')
            ->add('TYPCPTR_CODE')
            ->add('TYPCPTR_TYPE')
            ->add('TYPCPTR_NBTARIF')
            ->add('TYPCPTR_TARIF1')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RELEVE::class,
        ]);
    }
}
