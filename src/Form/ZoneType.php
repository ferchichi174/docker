<?php

namespace App\Form;

use App\Entity\ZONE;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ZoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ZONE_LIBELLE')
            ->add('RC_BT_CHARGMIL_CODE')
            ->add('ZONE_NBCPTR')
            ->add('ZONE_NBPOSITION')
            ->add('zone_chargcptr')
            ->add('ZONE_CHARGAGENT')
            ->add('ZONE_PARTIE')
            ->add('ZONE_ORDPQ')
            ->add('ZONE_NUMPQ')
            ->add('ZONE_OBSERV')
            ->add('ZONE_DATMAJ')
            ->add('zone_uf_code')
            ->add('ZONE_IDUTILISATEUR')
            ->add('DIST_ID')
            ->add('LOC')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ZONE::class,
        ]);
    }
}
