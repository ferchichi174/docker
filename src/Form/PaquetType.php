<?php

namespace App\Form;

use App\Entity\PAQUET;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaquetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('PQ_DATTRAIT')
            ->add('UH_USR_ID')
            ->add('PQ_NUM')
            ->add('PQ_DATMAJ')
            ->add('PQ_UFCODE')
            ->add('PQ_IDUTILISATEUR')
            ->add('LOC')
            ->add('DIST')
            ->add('ZONE')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PAQUET::class,
        ]);
    }
}
