<?php

namespace App\Form;

use App\Entity\DIST;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DistrictType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('DIST_CODREG')
            ->add('DIST_LIBELLEFR')
            ->add('DIST_LIBELLEAR')
            ->add('DIST_TEL')
            ->add('DIST_TOURNEEDEBUT')
            ->add('DIST_TOURNEEFIN')
            ->add('DIST_RIB')
            ->add('DIST_FAX')
            ->add('DIST_TDEBMTZ')
            ->add('DIST_VERSION')
            ->add('DIST_IDUTILISATEUR')
            ->add('DIST_TDEBMTZRF')
            ->add('DIST_TFINMT2RF')
            ->add('DIST_NUMAFFILIATION')
            ->add('DIST_TYPE')
            ->add('DIST_POLE')
            ->add('DIST_TELTECH')
            ->add('DIST_CODENEW')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DIST::class,
        ]);
    }
}
