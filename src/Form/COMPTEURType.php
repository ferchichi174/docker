<?php

namespace App\Form;

use App\Entity\COMPTEUR;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class COMPTEURType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('CF_AB_CODE')
            ->add('CF_AB_REF')
            ->add('RC_BTTARIF')
            ->add('CPTEUR_NUM')
            ->add('RC_CCPTEUR_CODE')
            ->add('CAPTEUR_DATEMES',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_DATEPOS',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_AVCONSO')
            ->add('CPTEUR_DATMAJ',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_UFCODE')
            ->add('CPTEUR_IDUTILISATEUR')
            ->add('RC_BT_EMPCPTR_ACCES')
            ->add('RC_BT_EMPCPTR_CODE')
            ->add('RC_BT_MARQCPTR_CODE')
            ->add('RC_BT_TYPCPTR_CODE')
            ->add('RC_BT_TYPCPTR_TYPE')
            ->add('RC_BT_TYPCPTR_NB_TARIF')
            ->add('RC_BT_TYPCPTR_NB_TARIF1')
            ->add('RC_BT_SCPTR_CODE')
            ->add('CPTEUR_DATEDEPOSE',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_COEFF')
            ->add('CPTEUR_DINDEX')
            ->add('CPTEUR_LOCK')
            ->add('CPTEUR_PINSTALLEE')          ->add('CF_AB_CODE')
            ->add('CF_AB_REF')
            ->add('RC_BTTARIF')
            ->add('CPTEUR_NUM')
            ->add('RC_CCPTEUR_CODE')
            ->add('CAPTEUR_DATEMES',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_DATEPOS',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_AVCONSO')
            ->add('CPTEUR_DATMAJ',DateType::class, [
        'widget' => 'single_text',
        'format' => 'yyyy-MM-dd',
    ])
            ->add('CPTEUR_UFCODE')
            ->add('CPTEUR_IDUTILISATEUR')
            ->add('RC_BT_EMPCPTR_ACCES')
            ->add('RC_BT_EMPCPTR_CODE')
            ->add('RC_BT_MARQCPTR_CODE')
            ->add('RC_BT_TYPCPTR_CODE')
            ->add('RC_BT_TYPCPTR_TYPE')
            ->add('RC_BT_TYPCPTR_NB_TARIF')
            ->add('RC_BT_TYPCPTR_NB_TARIF1')
            ->add('RC_BT_SCPTR_CODE')
            ->add('CPTEUR_DATEDEPOSE',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('CPTEUR_COEFF')
            ->add('CPTEUR_DINDEX')
            ->add('CPTEUR_LOCK')
            ->add('CPTEUR_PINSTALLEE')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => COMPTEUR::class,
        ]);
    }
}
