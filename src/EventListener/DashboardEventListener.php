<?php

namespace App\EventListener;

use Sonata\AdminBundle\Event\ConfigureEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DashboardEventListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'sonata.admin.event.configure.dashboard' => 'onDashboardRender',
        ];
    }

    public function onDashboardRender(ConfigureEvent $event)
    {
        $data = $event->getData();
        // Ajoutez vos variables à la vue dashboard.html.twig
        $data['variable1'] = 'valeur1';
        $data['variable2'] = 'valeur2';
        $event->setData($data);
    }
}