<?php

namespace App\Block;



use App\Entity\ABONNE;
use App\Repository\ABONNERepository;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\Service\BlockServiceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class DashblockService extends AbstractBlockService implements BlockServiceInterface
{
    /**
     * @var ABONNERepository
     */
    private $abonneRepository;

    public function __construct(Environment $twig, ABONNERepository $abonneRepository)
    {
        parent::__construct($twig);
        $this->abonneRepository = $abonneRepository;

    }



    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

            'title' => 'qsdqs',
            'template' => 'sonata/admin/dashblock.html.twig',
            'extraVariable' => "sdfsd",
        ]);
    }


    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {
        $abonnes = $this->getAllAbonnes();
        $settings = $blockContext->getSettings();

        return   $this->renderPrivateResponse($blockContext->getTemplate(), [
            'title' => $settings['title'],
            'extraVariable' => $settings['extraVariable'],
            'settings' => $settings,
            'abonnes' => $abonnes,
        ],$response);




    }

    private function getAllAbonnes()
    {
        $ab= $this->abonneRepository->findAll();
        return $ab;
    }

}
