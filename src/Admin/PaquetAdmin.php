<?php


namespace App\Admin;

use App\Entity\ABONNE;
use App\Entity\DIST;
use App\Entity\PAQUET;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class PaquetAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof PAQUET
            ? $object->getPQUFCODE()
            : 'PAQUET'; // shown in the breadcrumb on the create view
    }


    protected function configureFormFields(FormMapper $form): void
    {
        $form
         ->add('PQ_DATTRAIT', DateType::class, [
             'widget' => 'single_text',
             'format' => 'yyyy-MM-dd',
         ])
        ->add('UH_USR_ID')
        ->add('PQ_NUM')
        ->add('PQ_DATMAJ', DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('PQ_UFCODE')
        ->add('PQ_IDUTILISATEUR')
        ->add('LOC')
        ->add('DIST')
        ->add('ZONE');
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('PQ_DATTRAIT')
            ->add('UH_USR_ID')
            ->add('PQ_NUM')
            ->add('PQ_DATMAJ')
            ->add('PQ_UFCODE')
            ->add('PQ_IDUTILISATEUR')
            ->add('LOC')
            ->add('DIST')
            ->add('ZONE');

    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
        ->add('PQ_DATTRAIT')
        ->add('UH_USR_ID')
        ->add('PQ_NUM')
        ->add('PQ_DATMAJ')
        ->add(ListMapper::NAME_ACTIONS, null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ]]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
        ->add('PQ_DATTRAIT')
        ->add('UH_USR_ID')
        ->add('PQ_NUM')
        ->add('PQ_DATMAJ')
        ->add('PQ_UFCODE')
        ->add('PQ_IDUTILISATEUR')
        ->add('LOC')
        ->add('DIST')
        ->add('ZONE');
    }
}