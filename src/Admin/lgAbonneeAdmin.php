<?php

namespace App\Admin;

use App\Entity\ABONNE;
use App\Entity\LIGNEABONNE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class lgAbonneeAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof LIGNEABONNE
            ? $object->getCPTEURNUM()
            : 'Numero de capteur'; // shown in the breadcrumb on the create view
    }
    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('LGABONNE_TARIF', TextType::class);
        $form->add('CPTEUR_NUM', TextType::class);
        $form->add('RC_CCPTEUR_CODE', TextType::class);
        $form->add('amperage', TextType::class);
        $form->add('coeff', TextType::class);
        $form->add('lusage', TextType::class);
        $form->add('LIGNE_ABONNE_ETATCOMPTEUR', TextType::class);
        $form->add('LIGNE_ABONNE_ANCIENINDEX', TextType::class);
        $form->add('LIGNE_ABONNE_NBRREMOIS', TextType::class);
        $form->add('ABONNE', ModelType::class, [
        'class' => ABONNE::class,
        'property' => 'AbonneNom',
    ])
    ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {

        $datagrid->add('id');
        $datagrid->add('LGABONNE_TARIF');
        $datagrid->add('CPTEUR_NUM');
        $datagrid->add('RC_CCPTEUR_CODE');
        $datagrid->add('amperage');
        $datagrid->add('coeff');
        $datagrid->add('lusage');
        $datagrid->add('LIGNE_ABONNE_ETATCOMPTEUR');
        $datagrid->add('LIGNE_ABONNE_ANCIENINDEX');
        $datagrid->add('LIGNE_ABONNE_NBRREMOIS');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->add('id', TextType::class);
        $list->add('LGABONNE_TARIF', TextType::class);
        $list->add('CPTEUR_NUM', TextType::class);
        $list->add('RC_CCPTEUR_CODE', TextType::class);
        $list->add('ABONNE', TextType::class);
        $list ->add(ListMapper::NAME_ACTIONS, null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ]]);

    }


    protected function configureShowFields(ShowMapper $show): void
    {

        $show
            ->tab('Post')
        ->add('id')
        ->add('LGABONNE_TARIF')
        ->add('CPTEUR_NUM')
        ->add('RC_CCPTEUR_CODE')
        ->add('amperage')
        ->add('coeff')
        ->add('lusage')
        ->add('LIGNE_ABONNE_ETATCOMPTEUR')
        ->add('LIGNE_ABONNE_ANCIENINDEX')
        ->add('LIGNE_ABONNE_NBRREMOIS')
        ->end();
    }
}