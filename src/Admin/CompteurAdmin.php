<?php


namespace App\Admin;

use App\Entity\ABONNE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CompteurAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof ABONNE
            ? $object->getCFABCODE()
            : 'Blog Post'; // shown in the breadcrumb on the create view
    }



    protected function configureFormFields(FormMapper $form): void
    {
    $form
        ->add('CF_AB_CODE')
        ->add('CF_AB_REF')
        ->add('RC_BTTARIF')
        ->add('CPTEUR_NUM')
        ->add('RC_CCPTEUR_CODE')
        ->add('CAPTEUR_DATEMES',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_DATEPOS',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_AVCONSO')
        ->add('CPTEUR_DATMAJ',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_UFCODE')
        ->add('CPTEUR_IDUTILISATEUR')
        ->add('RC_BT_EMPCPTR_ACCES')
        ->add('RC_BT_EMPCPTR_CODE')
        ->add('RC_BT_MARQCPTR_CODE')
        ->add('RC_BT_TYPCPTR_CODE')
        ->add('RC_BT_TYPCPTR_TYPE')
        ->add('RC_BT_TYPCPTR_NB_TARIF')
        ->add('RC_BT_TYPCPTR_NB_TARIF1')
        ->add('RC_BT_SCPTR_CODE')
        ->add('CPTEUR_DATEDEPOSE',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_COEFF')
        ->add('CPTEUR_DINDEX')
        ->add('CPTEUR_LOCK')
        ->add('CPTEUR_PINSTALLEE')          ->add('CF_AB_CODE')
        ->add('CF_AB_REF')
        ->add('RC_BTTARIF')
        ->add('CPTEUR_NUM')
        ->add('RC_CCPTEUR_CODE')
        ->add('CAPTEUR_DATEMES',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_DATEPOS',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_AVCONSO')
        ->add('CPTEUR_DATMAJ',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_UFCODE')
        ->add('CPTEUR_IDUTILISATEUR')
        ->add('RC_BT_EMPCPTR_ACCES')
        ->add('RC_BT_EMPCPTR_CODE')
        ->add('RC_BT_MARQCPTR_CODE')
        ->add('RC_BT_TYPCPTR_CODE')
        ->add('RC_BT_TYPCPTR_TYPE')
        ->add('RC_BT_TYPCPTR_NB_TARIF')
        ->add('RC_BT_TYPCPTR_NB_TARIF1')
        ->add('RC_BT_SCPTR_CODE')
        ->add('CPTEUR_DATEDEPOSE',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('CPTEUR_COEFF')
        ->add('CPTEUR_DINDEX')
        ->add('CPTEUR_LOCK')
        ->add('CPTEUR_PINSTALLEE');
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
    $datagrid
        ->add('CF_AB_CODE')
        ->add('CF_AB_REF')
        ->add('RC_BTTARIF')
        ->add('CPTEUR_NUM')
        ->add('RC_CCPTEUR_CODE')
        ->add('CAPTEUR_DATEMES')
        ->add('CPTEUR_DATEPOS')
        ->add('CPTEUR_AVCONSO')
        ->add('CPTEUR_DATMAJ')
        ->add('CPTEUR_UFCODE')
        ->add('CPTEUR_IDUTILISATEUR')
        ->add('RC_BT_EMPCPTR_ACCES')
        ->add('RC_BT_EMPCPTR_CODE')
        ->add('RC_BT_MARQCPTR_CODE')
        ->add('RC_BT_TYPCPTR_CODE')
        ->add('RC_BT_TYPCPTR_TYPE')
        ->add('RC_BT_TYPCPTR_NB_TARIF')
        ->add('RC_BT_TYPCPTR_NB_TARIF1')
        ->add('RC_BT_SCPTR_CODE')
        ->add('CPTEUR_DATEDEPOSE')
        ->add('CPTEUR_COEFF')
        ->add('CPTEUR_DINDEX')
        ->add('CPTEUR_LOCK')
        ->add('CPTEUR_PINSTALLEE');


    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('CF_AB_CODE')
            ->add('CF_AB_REF')
            ->add('RC_BTTARIF')
            ->add('CPTEUR_NUM')
            ->add('RC_CCPTEUR_CODE')
            ->add('CAPTEUR_DATEMES')
            ->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [],
                'edit' => [],
                'delete' => [],
            ]]);



    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('CF_AB_CODE')
            ->add('CF_AB_REF')
            ->add('RC_BTTARIF')
            ->add('CPTEUR_NUM')
            ->add('RC_CCPTEUR_CODE')
            ->add('CAPTEUR_DATEMES')
            ->add('CPTEUR_DATEPOS')
            ->add('CPTEUR_AVCONSO')
            ->add('CPTEUR_DATMAJ')
            ->add('CPTEUR_UFCODE')
            ->add('CPTEUR_IDUTILISATEUR')
            ->add('RC_BT_EMPCPTR_ACCES')
            ->add('RC_BT_EMPCPTR_CODE')
            ->add('RC_BT_MARQCPTR_CODE')
            ->add('RC_BT_TYPCPTR_CODE')
            ->add('RC_BT_TYPCPTR_TYPE')
            ->add('RC_BT_TYPCPTR_NB_TARIF')
            ->add('RC_BT_TYPCPTR_NB_TARIF1')
            ->add('RC_BT_SCPTR_CODE')
            ->add('CPTEUR_DATEDEPOSE')
            ->add('CPTEUR_COEFF')
            ->add('CPTEUR_DINDEX')
            ->add('CPTEUR_LOCK')
            ->add('CPTEUR_PINSTALLEE');


    }
}