<?php


namespace App\Admin;

use App\Entity\ZONE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ZoneAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof ZONE
            ? $object->getZONELIBELE()
            : 'Blog Post'; // shown in the breadcrumb on the create view
    }


    protected function configureFormFields(FormMapper $form): void
    {
        $form


        ->add('ZONE_LIBELLE')
        ->add('RC_BT_CHARGMIL_CODE')
        ->add('ZONE_NBCPTR')
        ->add('ZONE_NBPOSITION')
        ->add('zone_chargcptr')
        ->add('ZONE_CHARGAGENT')
        ->add('ZONE_PARTIE')
        ->add('ZONE_ORDPQ')
        ->add('ZONE_NUMPQ')
        ->add('ZONE_OBSERV')
        ->add('ZONE_DATMAJ',DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('zone_uf_code')
        ->add('ZONE_IDUTILISATEUR')
        ->add('DIST_ID')
        ->add('LOC');
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('ZONE_LIBELLE')
        ->add('RC_BT_CHARGMIL_CODE')
        ->add('ZONE_NBCPTR')
        ->add('ZONE_NBPOSITION')
        ->add('zone_chargcptr')
        ->add('ZONE_CHARGAGENT')
        ->add('ZONE_PARTIE')
        ->add('ZONE_ORDPQ')
        ->add('ZONE_NUMPQ')
        ->add('ZONE_OBSERV')
        ->add('ZONE_DATMAJ')
        ->add('zone_uf_code')
        ->add('ZONE_IDUTILISATEUR')
        ->add('DIST_ID')
        ->add('LOC');

    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->add('id')->add('ZONE_LIBELLE')
        ->add('RC_BT_CHARGMIL_CODE')
        ->add('ZONE_NBCPTR')
        ->add('ZONE_NBPOSITION')
        ->add('zone_chargcptr')
        ->add(ListMapper::NAME_ACTIONS, null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ]]);

    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('ZONE_LIBELLE')
        ->add('RC_BT_CHARGMIL_CODE')
        ->add('ZONE_NBCPTR')
        ->add('ZONE_NBPOSITION')
        ->add('zone_chargcptr')
        ->add('ZONE_CHARGAGENT')
        ->add('ZONE_PARTIE')
        ->add('ZONE_ORDPQ')
        ->add('ZONE_NUMPQ')
        ->add('ZONE_OBSERV')
        ->add('ZONE_DATMAJ')
        ->add('zone_uf_code')
        ->add('ZONE_IDUTILISATEUR')
        ->add('DIST_ID')
        ->add('LOC');
    }
}