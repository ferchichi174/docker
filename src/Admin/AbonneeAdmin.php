<?php


namespace App\Admin;

use App\Entity\ABONNE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class AbonneeAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof ABONNE
            ? $object->getABONNENUMSEQ()
            : 'Blog Post'; // shown in the breadcrumb on the create view
    }



protected function configureFormFields(FormMapper $form): void
{
$form->add('ABONNE_NUMSEQ', TextType::class);
$form->add('ABONNE_CODEUR', TextType::class);
$form->add('ABONNE_DTEDITION', DateType::class, [
    'widget' => 'single_text',
    'format' => 'yyyy-MM-dd',
]);
$form->add('ABONNE_DTRELEVE', DateType::class, [
    'widget' => 'single_text',
    'format' => 'yyyy-MM-dd',
]);
$form->add('ABONNE_REFCLT', TextType::class);
$form->add('ZONE_CODE', TextType::class);
$form->add('ABONNE_POSITION', TextType::class);
$form->add('abonne_etat', TextType::class);
$form->add('abonne_adr', TextType::class);
$form->add('abonne_nom', TextType::class);
$form->add('abonne_usage', TextType::class);
}

protected function configureDatagridFilters(DatagridMapper $datagrid): void
{
$datagrid->add('ABONNE_NUMSEQ');
$datagrid->add('ZONE_CODE');
$datagrid->add('ABONNE_POSITION');
$datagrid->add('abonne_etat');
$datagrid->add('abonne_adr');
$datagrid->add('abonne_nom');
$datagrid->add('abonne_usage');
}

protected function configureListFields(ListMapper $list): void
{
$list->addIdentifier('id');
$list->addIdentifier('ABONNE_NUMSEQ');
$list->addIdentifier('ZONE_CODE');
$list->addIdentifier('ABONNE_POSITION');
$list->addIdentifier('abonne_etat');
$list->addIdentifier('abonne_adr');
$list->addIdentifier('abonne_nom');
$list->addIdentifier('abonne_usage');
    $list ->addIdentifier(ListMapper::NAME_ACTIONS, null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ]]);
}

protected function configureShowFields(ShowMapper $show): void
{
$show->add('id');
$show->add('ABONNE_NUMSEQ');
$show->add('ZONE_CODE');
$show->add('ABONNE_POSITION');
$show->add('abonne_etat');
$show->add('abonne_adr');
$show->add('abonne_nom');
$show->add('abonne_usage');
    $show ->add(ListMapper::NAME_ACTIONS, null, [
    'actions' => [
        'show' => [],
        'edit' => [],
        'delete' => [],
    ]
]);
}

}