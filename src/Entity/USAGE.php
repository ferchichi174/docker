<?php

namespace App\Entity;

use App\Repository\USAGERepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: USAGERepository::class)]
#[ORM\Table(name: '`usage`')]
class USAGE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`usage_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $CODE_USAGE = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIB_USAGE = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODEUSAGE(): ?string
    {
        return $this->CODE_USAGE;
    }

    public function setCODEUSAGE(?string $CODE_USAGE): self
    {
        $this->CODE_USAGE = $CODE_USAGE;

        return $this;
    }

    public function getLIBUSAGE(): ?string
    {
        return $this->LIB_USAGE;
    }

    public function setLIBUSAGE(?string $LIB_USAGE): self
    {
        $this->LIB_USAGE = $LIB_USAGE;

        return $this;
    }
}
