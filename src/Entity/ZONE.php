<?php

namespace App\Entity;

use App\Repository\ZONERepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ZONERepository::class)]
class ZONE
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`zone_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    public ?string $ZONE_LIBELLE = null;

    #[ORM\Column(length: 1)]
    private ?string $RC_BT_CHARGMIL_CODE = null;


    #[ORM\Column(nullable: true)]
    private ?int $ZONE_NBCPTR = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_NBPOSITION = null;

    #[ORM\Column(nullable: true)]
    private ?int $zone_chargcptr = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_CHARGAGENT = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_PARTIE = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_ORDPQ = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_NUMPQ = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $ZONE_OBSERV = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $ZONE_DATMAJ = null;

    #[ORM\Column(length: 4, nullable: true)]
    private ?string $zone_uf_code = null;

    #[ORM\Column(nullable: true)]
    private ?int $ZONE_IDUTILISATEUR = null;

    #[ORM\ManyToOne(inversedBy: 'zONEs')]
    #[ORM\JoinColumn(name: 'dist_id', referencedColumnName: 'dist_id')]
    private ?DIST $DIST_ID = null;

    #[ORM\ManyToOne(inversedBy: 'zONEs')]
    #[ORM\JoinColumn(name: 'loc_id', referencedColumnName: 'loc_id')]
    private ?LOCALITE $LOC = null;

    #[ORM\OneToMany(mappedBy: 'ZONE', targetEntity: PAQUET::class)]
    private Collection $PAQUETS;




    public function __construct()
    {
        $this->PAQUETS = new ArrayCollection();
    }
    public function __toString(){
        return $this->ZONE_LIBELLE;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZONELIBELE(): ?string
    {
        return $this->ZONE_LIBELLE;
    }

    public function setZONELIBELE(string $ZONE_LIBELE): self
    {
        $this->ZONE_LIBELLE = $ZONE_LIBELE;

        return $this;
    }

    public function getRCBTCHARGMILCODE(): ?string
    {
        return $this->RC_BT_CHARGMIL_CODE;
    }

    public function setRCBTCHARGMILCODE(string $RC_BT_CHARGMIL_CODE): self
    {
        $this->RC_BT_CHARGMIL_CODE = $RC_BT_CHARGMIL_CODE;

        return $this;
    }

    public function getLOCCODE(): ?LOCALITE
    {
        return $this->LOC_CODE;
    }

    public function setLOCCODE(?LOCALITE $LOC_CODE): self
    {
        $this->LOC_CODE = $LOC_CODE;

        return $this;
    }

    public function getDISTCODE(): ?DIST
    {
        return $this->DIST_CODE;
    }

    public function setDISTCODE(?DIST $DIST_CODE): self
    {
        $this->DIST_CODE = $DIST_CODE;

        return $this;
    }




    public function getZONENBCPTR(): ?int
    {
        return $this->ZONE_NBCPTR;
    }

    public function setZONENBCPTR(?int $ZONE_NBCPTR): self
    {
        $this->ZONE_NBCPTR = $ZONE_NBCPTR;

        return $this;
    }

    public function getZONENBPOSITION(): ?int
    {
        return $this->ZONE_NBPOSITION;
    }

    public function setZONENBPOSITION(?int $ZONE_NBPOSITION): self
    {
        $this->ZONE_NBPOSITION = $ZONE_NBPOSITION;

        return $this;
    }

    public function getZoneChargcptr(): ?int
    {
        return $this->zone_chargcptr;
    }

    public function setZoneChargcptr(?int $zone_chargcptr): self
    {
        $this->zone_chargcptr = $zone_chargcptr;

        return $this;
    }

    public function getZONECHARGAGENT(): ?int
    {
        return $this->ZONE_CHARGAGENT;
    }

    public function setZONECHARGAGENT(?int $ZONE_CHARGAGENT): self
    {
        $this->ZONE_CHARGAGENT = $ZONE_CHARGAGENT;

        return $this;
    }

    public function getZONEPARTIE(): ?int
    {
        return $this->ZONE_PARTIE;
    }

    public function setZONEPARTIE(?int $ZONE_PARTIE): self
    {
        $this->ZONE_PARTIE = $ZONE_PARTIE;

        return $this;
    }

    public function getZONEORDPQ(): ?int
    {
        return $this->ZONE_ORDPQ;
    }

    public function setZONEORDPQ(?int $ZONE_ORDPQ): self
    {
        $this->ZONE_ORDPQ = $ZONE_ORDPQ;

        return $this;
    }

    public function getZONENUMPQ(): ?int
    {
        return $this->ZONE_NUMPQ;
    }

    public function setZONENUMPQ(?int $ZONE_NUMPQ): self
    {
        $this->ZONE_NUMPQ = $ZONE_NUMPQ;

        return $this;
    }

    public function getZONEOBSERV(): ?string
    {
        return $this->ZONE_OBSERV;
    }

    public function setZONEOBSERV(?string $ZONE_OBSERV): self
    {
        $this->ZONE_OBSERV = $ZONE_OBSERV;

        return $this;
    }

    public function getZONEDATMAJ(): ?\DateTimeInterface
    {
        return $this->ZONE_DATMAJ;
    }

    public function setZONEDATMAJ(?\DateTimeInterface $ZONE_DATMAJ): self
    {
        $this->ZONE_DATMAJ = $ZONE_DATMAJ;

        return $this;
    }

    public function getZoneUfCode(): ?string
    {
        return $this->zone_uf_code;
    }

    public function setZoneUfCode(?string $zone_uf_code): self
    {
        $this->zone_uf_code = $zone_uf_code;

        return $this;
    }

    public function getZONEIDUTILISATEUR(): ?int
    {
        return $this->ZONE_IDUTILISATEUR;
    }

    public function setZONEIDUTILISATEUR(?int $ZONE_IDUTILISATEUR): self
    {
        $this->ZONE_IDUTILISATEUR = $ZONE_IDUTILISATEUR;

        return $this;
    }

    public function getDISTID(): ?DIST
    {
        return $this->DIST_ID;
    }

    public function setDISTID(?DIST $DIST_ID): self
    {
        $this->DIST_ID = $DIST_ID;

        return $this;
    }

    public function getLOC(): ?LOCALITE
    {
        return $this->LOC;
    }

    public function setLOC(?LOCALITE $LOC): self
    {
        $this->LOC = $LOC;

        return $this;
    }

    /**
     * @return Collection<int, PAQUET>
     */
    public function getPAQUETS(): Collection
    {
        return $this->PAQUETS;
    }

    public function addPAQUET(PAQUET $pAQUET): self
    {
        if (!$this->PAQUETS->contains($pAQUET)) {
            $this->PAQUETS->add($pAQUET);
            $pAQUET->setZONE($this);
        }

        return $this;
    }

    public function removePAQUET(PAQUET $pAQUET): self
    {
        if ($this->PAQUETS->removeElement($pAQUET)) {
            // set the owning side to null (unless already changed)
            if ($pAQUET->getZONE() === $this) {
                $pAQUET->setZONE(null);
            }
        }

        return $this;
    }








}
