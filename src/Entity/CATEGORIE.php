<?php

namespace App\Entity;

use App\Repository\CATEGORIERepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CATEGORIERepository::class)]
class CATEGORIE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`categorie_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $CODE_CATEGORIE = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIB_CATEGORIE = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODECATEGORIE(): ?string
    {
        return $this->CODE_CATEGORIE;
    }

    public function setCODECATEGORIE(?string $CODE_CATEGORIE): self
    {
        $this->CODE_CATEGORIE = $CODE_CATEGORIE;

        return $this;
    }

    public function getLIBCATEGORIE(): ?string
    {
        return $this->LIB_CATEGORIE;
    }

    public function setLIBCATEGORIE(?string $LIB_CATEGORIE): self
    {
        $this->LIB_CATEGORIE = $LIB_CATEGORIE;

        return $this;
    }
}
