<?php

namespace App\Entity;

use App\Repository\DISTRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DISTRepository::class)]
class DIST
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`dist_id`', length:2)]
    private ?int $id = null;

    #[ORM\Column(length: 2)]
    private ?string $DIST_CODREG = null;

    #[ORM\Column(length: 50)]
    private ?string $DIST_LIBELLEFR = null;

    #[ORM\Column(length: 50)]
    private ?string $DIST_LIBELLEAR = null;

    #[ORM\Column(length: 8)]
    private ?string $DIST_TEL = null;

    #[ORM\Column(length: 5)]
    private ?string $DIST_TOURNEEDEBUT = null;

    #[ORM\Column(length: 5)]
    private ?string $DIST_TOURNEEFIN = null;

    #[ORM\Column(length: 20)]
    private ?string $DIST_RIB = null;

    #[ORM\Column(length: 8)]
    private ?string $DIST_FAX = null;

    #[ORM\Column(length: 3)]
    private ?string $DIST_TDEBMTZ = null;

    #[ORM\Column(length: 3)]
    private ?string $DIST_VERSION = null;

    #[ORM\Column(length: 5)]
    private ?string $DIST_IDUTILISATEUR = null;

    #[ORM\Column(length: 3)]
    private ?string $DIST_TDEBMTZRF = null;

    #[ORM\Column(length: 3)]
    private ?string $DIST_TFINMT2RF = null;

    #[ORM\Column(length: 8)]
    private ?string $DIST_NUMAFFILIATION = null;

    #[ORM\Column(length: 1)]
    private ?string $DIST_TYPE = null;

    #[ORM\Column(length: 1)]
    private ?string $DIST_POLE = null;

    #[ORM\Column(length: 8)]
    private ?string $DIST_TELTECH = null;

    #[ORM\Column(length: 3)]
    private ?string $DIST_CODENEW = null;


    #[ORM\OneToMany(mappedBy: 'DIST', targetEntity: ZONE::class)]
    private Collection $ZONES;


    #[ORM\OneToMany(mappedBy: 'DIST_CODE', targetEntity: LOCALITE::class)]
    private Collection $LOCALITES;

    #[ORM\OneToMany(mappedBy: 'DIST_ID', targetEntity: ZONE::class)]
    private Collection $zONEs;

    #[ORM\OneToMany(mappedBy: 'DIST', targetEntity: PAQUET::class)]
    private Collection $pAQUETs;





    public function __construct()
    {
        $this->ZONES = new ArrayCollection();
        $this->lOCALITEs = new ArrayCollection();
        $this->zONEs = new ArrayCollection();
        $this->PAQUETS = new ArrayCollection();
        $this->LOCALITES = new ArrayCollection();
        $this->local = new ArrayCollection();
        $this->pAQUETs = new ArrayCollection();
    }

    public function __toString(){
        return $this->DIST_CODREG;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDISTCODREG(): ?string
    {
        return $this->DIST_CODREG;
    }

    public function setDISTCODREG(string $DIST_CODREG): self
    {
        $this->DIST_CODREG = $DIST_CODREG;

        return $this;
    }

    public function getDISTLIBELLEFR(): ?string
    {
        return $this->DIST_LIBELLEFR;
    }

    public function setDISTLIBELLEFR(string $DIST_LIBELLEFR): self
    {
        $this->DIST_LIBELLEFR = $DIST_LIBELLEFR;

        return $this;
    }

    public function getDISTLIBELLEAR(): ?string
    {
        return $this->DIST_LIBELLEAR;
    }

    public function setDISTLIBELLEAR(string $DIST_LIBELLEAR): self
    {
        $this->DIST_LIBELLEAR = $DIST_LIBELLEAR;

        return $this;
    }

    public function getDISTTEL(): ?string
    {
        return $this->DIST_TEL;
    }

    public function setDISTTEL(string $DIST_TEL): self
    {
        $this->DIST_TEL = $DIST_TEL;

        return $this;
    }

    public function getDISTTOURNEEDEBUT(): ?string
    {
        return $this->DIST_TOURNEEDEBUT;
    }

    public function setDISTTOURNEEDEBUT(string $DIST_TOURNEEDEBUT): self
    {
        $this->DIST_TOURNEEDEBUT = $DIST_TOURNEEDEBUT;

        return $this;
    }

    public function getDISTTOURNEEFIN(): ?string
    {
        return $this->DIST_TOURNEEFIN;
    }

    public function setDISTTOURNEEFIN(string $DIST_TOURNEEFIN): self
    {
        $this->DIST_TOURNEEFIN = $DIST_TOURNEEFIN;

        return $this;
    }

    public function getDISTRIB(): ?string
    {
        return $this->DIST_RIB;
    }

    public function setDISTRIB(string $DIST_RIB): self
    {
        $this->DIST_RIB = $DIST_RIB;

        return $this;
    }

    public function getDISTFAX(): ?string
    {
        return $this->DIST_FAX;
    }

    public function setDISTFAX(string $DIST_FAX): self
    {
        $this->DIST_FAX = $DIST_FAX;

        return $this;
    }

    public function getDISTTDEBMTZ(): ?string
    {
        return $this->DIST_TDEBMTZ;
    }

    public function setDISTTDEBMTZ(string $DIST_TDEBMTZ): self
    {
        $this->DIST_TDEBMTZ = $DIST_TDEBMTZ;

        return $this;
    }

    public function getDISTVERSION(): ?string
    {
        return $this->DIST_VERSION;
    }

    public function setDISTVERSION(string $DIST_VERSION): self
    {
        $this->DIST_VERSION = $DIST_VERSION;

        return $this;
    }

    public function getDISTIDUTILISATEUR(): ?string
    {
        return $this->DIST_IDUTILISATEUR;
    }

    public function setDISTIDUTILISATEUR(string $DIST_IDUTILISATEUR): self
    {
        $this->DIST_IDUTILISATEUR = $DIST_IDUTILISATEUR;

        return $this;
    }

    public function getDISTTDEBMTZRF(): ?string
    {
        return $this->DIST_TDEBMTZRF;
    }

    public function setDISTTDEBMTZRF(string $DIST_TDEBMTZRF): self
    {
        $this->DIST_TDEBMTZRF = $DIST_TDEBMTZRF;

        return $this;
    }

    public function getDISTTFINMT2RF(): ?string
    {
        return $this->DIST_TFINMT2RF;
    }

    public function setDISTTFINMT2RF(string $DIST_TFINMT2RF): self
    {
        $this->DIST_TFINMT2RF = $DIST_TFINMT2RF;

        return $this;
    }

    public function getDISTNUMAFFILIATION(): ?string
    {
        return $this->DIST_NUMAFFILIATION;
    }

    public function setDISTNUMAFFILIATION(string $DIST_NUMAFFILIATION): self
    {
        $this->DIST_NUMAFFILIATION = $DIST_NUMAFFILIATION;

        return $this;
    }

    public function getDISTTYPE(): ?string
    {
        return $this->DIST_TYPE;
    }

    public function setDISTTYPE(string $DIST_TYPE): self
    {
        $this->DIST_TYPE = $DIST_TYPE;

        return $this;
    }

    public function getDISTPOLE(): ?string
    {
        return $this->DIST_POLE;
    }

    public function setDISTPOLE(string $DIST_POLE): self
    {
        $this->DIST_POLE = $DIST_POLE;

        return $this;
    }

    public function getDISTTELTECH(): ?string
    {
        return $this->DIST_TELTECH;
    }

    public function setDISTTELTECH(string $DIST_TELTECH): self
    {
        $this->DIST_TELTECH = $DIST_TELTECH;

        return $this;
    }

    public function getDISTCODENEW(): ?string
    {
        return $this->DIST_CODENEW;
    }

    public function setDISTCODENEW(string $DIST_CODENEW): self
    {
        $this->DIST_CODENEW = $DIST_CODENEW;

        return $this;
    }




    /**
     * @return Collection<int, LOCALITE>
     */
    public function getLOCALITES(): Collection
    {
        return $this->LOCALITES;
    }

    public function addLOCALITE(LOCALITE $lOCALITE): self
    {
        if (!$this->LOCALITES->contains($lOCALITE)) {
            $this->LOCALITES->add($lOCALITE);
            $lOCALITE->setDISTCODE($this);
        }

        return $this;
    }

    public function removeLOCALITE(LOCALITE $lOCALITE): self
    {
        if ($this->LOCALITES->removeElement($lOCALITE)) {
            // set the owning side to null (unless already changed)
            if ($lOCALITE->getDISTCODE() === $this) {
                $lOCALITE->setDISTCODE(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ZONE>
     */
    public function getZONEs(): Collection
    {
        return $this->zONEs;
    }

    public function addZONE(ZONE $zONE): self
    {
        if (!$this->zONEs->contains($zONE)) {
            $this->zONEs->add($zONE);
            $zONE->setDISTID($this);
        }

        return $this;
    }

    public function removeZONE(ZONE $zONE): self
    {
        if ($this->zONEs->removeElement($zONE)) {
            // set the owning side to null (unless already changed)
            if ($zONE->getDISTID() === $this) {
                $zONE->setDISTID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PAQUET>
     */
    public function getPAQUETs(): Collection
    {
        return $this->pAQUETs;
    }

    public function addPAQUET(PAQUET $pAQUET): self
    {
        if (!$this->pAQUETs->contains($pAQUET)) {
            $this->pAQUETs->add($pAQUET);
            $pAQUET->setDIST($this);
        }

        return $this;
    }

    public function removePAQUET(PAQUET $pAQUET): self
    {
        if ($this->pAQUETs->removeElement($pAQUET)) {
            // set the owning side to null (unless already changed)
            if ($pAQUET->getDIST() === $this) {
                $pAQUET->setDIST(null);
            }
        }

        return $this;
    }




}
