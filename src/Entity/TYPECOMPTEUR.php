<?php

namespace App\Entity;

use App\Repository\TYPECOMPTEURRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TYPECOMPTEURRepository::class)]
#[ORM\Table(name: '`TYPE_COMPTEUR`')]
class TYPECOMPTEUR
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`type_cpt_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $TYPE = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIB = null;

    #[ORM\OneToMany(mappedBy: 'typcptr_type', targetEntity: RELEVE::class)]
    private Collection $RELEVES;

    public function __construct()
    {
        $this->RELEVES = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTYPE(): ?string
    {
        return $this->TYPE;
    }

    public function setTYPE(?string $TYPE): self
    {
        $this->TYPE = $TYPE;

        return $this;
    }

    public function getLIB(): ?string
    {
        return $this->LIB;
    }

    public function setLIB(?string $LIB): self
    {
        $this->LIB = $LIB;

        return $this;
    }

    /**
     * @return Collection<int, RELEVE>
     */
    public function getRELEVES(): Collection
    {
        return $this->RELEVES;
    }

    public function addRELEVE(RELEVE $rELEVE): self
    {
        if (!$this->RELEVES->contains($rELEVE)) {
            $this->RELEVES->add($rELEVE);
            $rELEVE->setTypcptrType($this);
        }

        return $this;
    }

    public function removeRELEVE(RELEVE $rELEVE): self
    {
        if ($this->RELEVES->removeElement($rELEVE)) {
            // set the owning side to null (unless already changed)
            if ($rELEVE->getTypcptrType() === $this) {
                $rELEVE->setTypcptrType(null);
            }
        }

        return $this;
    }
}
