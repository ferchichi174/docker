<?php

namespace App\Entity;

use App\Repository\ERREURRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ERREURRepository::class)]
class ERREUR
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`erreur_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $CODE_ERREUR = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIB_ERREUR = null;

    #[ORM\OneToMany(mappedBy: 'ERREUR', targetEntity: RELEVE::class)]
    private Collection $RELEVES;

    public function __construct()
    {
        $this->RELEVES = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODEERREUR(): ?string
    {
        return $this->CODE_ERREUR;
    }

    public function setCODEERREUR(?string $CODE_ERREUR): self
    {
        $this->CODE_ERREUR = $CODE_ERREUR;

        return $this;
    }

    public function getLIBERREUR(): ?string
    {
        return $this->LIB_ERREUR;
    }

    public function setLIBERREUR(?string $LIB_ERREUR): self
    {
        $this->LIB_ERREUR = $LIB_ERREUR;

        return $this;
    }

    /**
     * @return Collection<int, RELEVE>
     */
    public function getRELEVES(): Collection
    {
        return $this->RELEVES;
    }

    public function addRELEVE(RELEVE $rELEVE): self
    {
        if (!$this->RELEVES->contains($rELEVE)) {
            $this->RELEVES->add($rELEVE);
            $rELEVE->setERREUR($this);
        }

        return $this;
    }

    public function removeRELEVE(RELEVE $rELEVE): self
    {
        if ($this->RELEVES->removeElement($rELEVE)) {
            // set the owning side to null (unless already changed)
            if ($rELEVE->getERREUR() === $this) {
                $rELEVE->setERREUR(null);
            }
        }

        return $this;
    }
}
