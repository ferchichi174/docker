<?php

namespace App\Entity;

use App\Repository\ROLERepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ROLERepository::class)]
class ROLE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`role_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $ROLE_NAME = null;

    #[ORM\OneToOne(mappedBy: 'ROLE', cascade: ['persist', 'remove'])]
    private ?User $user = null;



    public function getid(): ?int
    {
        return $this->id;
    }

    public function getROLENAME(): ?string
    {
        return $this->ROLE_NAME;
    }

    public function setROLENAME(?string $ROLE_NAME): self
    {
        $this->ROLE_NAME = $ROLE_NAME;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setROLE(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getROLE() !== $this) {
            $user->setROLE($this);
        }

        $this->user = $user;

        return $this;
    }





}
