<?php

namespace App\Entity;

use App\Repository\TSPRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TSPRepository::class)]
class TSP
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`tsp_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    private ?string $TSP_REF = null;


    #[ORM\Column(length: 45, nullable: true)]
    private ?string $TSP_MARQUE = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $TSP_DTENTRY = null;

    #[ORM\OneToMany(mappedBy: 'Tsp', targetEntity: User::class)]
    private Collection $users;




    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function __toString(){
        return $this->TSP_REF;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTSPREF(): ?string
    {
        return $this->TSP_REF;
    }

    public function setTSPREF(string $TSP_REF): self
    {
        $this->TSP_REF = $TSP_REF;

        return $this;
    }


    public function getTSPMARQUE(): ?string
    {
        return $this->TSP_MARQUE;
    }

    public function setTSPMARQUE(?string $TSP_MARQUE): self
    {
        $this->TSP_MARQUE = $TSP_MARQUE;

        return $this;
    }

    public function getTSPDTENTRY(): ?\DateTimeInterface
    {
        return $this->TSP_DTENTRY;
    }

    public function setTSPDTENTRY(?\DateTimeInterface $TSP_DTENTRY): self
    {
        $this->TSP_DTENTRY = $TSP_DTENTRY;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setTsp($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getTsp() === $this) {
                $user->setTsp(null);
            }
        }

        return $this;
    }







}
