<?php

namespace App\Entity;

use App\Repository\POSTERepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: POSTERepository::class)]
class POSTE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`post_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 7, nullable: true)]
    private ?string $CODE_POSTE = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $NOM_POSTE = null;

    #[ORM\OneToMany(mappedBy: 'CODE_POSTE', targetEntity: COMPTEUR::class)]
    private Collection $COMPTEURS;

    public function __construct()
    {
        $this->COMPTEURS = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODEPOSTE(): ?string
    {
        return $this->CODE_POSTE;
    }

    public function setCODEPOSTE(?string $CODE_POSTE): self
    {
        $this->CODE_POSTE = $CODE_POSTE;

        return $this;
    }

    public function getNOMPOSTE(): ?string
    {
        return $this->NOM_POSTE;
    }

    public function setNOMPOSTE(?string $NOM_POSTE): self
    {
        $this->NOM_POSTE = $NOM_POSTE;

        return $this;
    }

    /**
     * @return Collection<int, COMPTEUR>
     */
    public function getCOMPTEURS(): Collection
    {
        return $this->COMPTEURS;
    }

    public function addCOMPTEUR(COMPTEUR $cOMPTEUR): self
    {
        if (!$this->COMPTEURS->contains($cOMPTEUR)) {
            $this->COMPTEURS->add($cOMPTEUR);
            $cOMPTEUR->setCODEPOSTE($this);
        }

        return $this;
    }

    public function removeCOMPTEUR(COMPTEUR $cOMPTEUR): self
    {
        if ($this->COMPTEURS->removeElement($cOMPTEUR)) {
            // set the owning side to null (unless already changed)
            if ($cOMPTEUR->getCODEPOSTE() === $this) {
                $cOMPTEUR->setCODEPOSTE(null);
            }
        }

        return $this;
    }
}
