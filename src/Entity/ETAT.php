<?php

namespace App\Entity;

use App\Repository\ETATRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ETATRepository::class)]
class ETAT
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`etat_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $CODE_ETAT = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIB_ETAT = null;

    #[ORM\OneToMany(mappedBy: 'etat', targetEntity: RELEVE::class)]
    private Collection $RELEVES;

    public function __construct()
    {
        $this->RELEVES = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODEETAT(): ?string
    {
        return $this->CODE_ETAT;
    }

    public function setCODEETAT(?string $CODE_ETAT): self
    {
        $this->CODE_ETAT = $CODE_ETAT;

        return $this;
    }

    public function getLIBETAT(): ?string
    {
        return $this->LIB_ETAT;
    }

    public function setLIBETAT(?string $LIB_ETAT): self
    {
        $this->LIB_ETAT = $LIB_ETAT;

        return $this;
    }

    /**
     * @return Collection<int, RELEVE>
     */
    public function getRELEVES(): Collection
    {
        return $this->RELEVES;
    }

    public function addRELEVE(RELEVE $rELEVE): self
    {
        if (!$this->RELEVES->contains($rELEVE)) {
            $this->RELEVES->add($rELEVE);
            $rELEVE->setEtat($this);
        }

        return $this;
    }

    public function removeRELEVE(RELEVE $rELEVE): self
    {
        if ($this->RELEVES->removeElement($rELEVE)) {
            // set the owning side to null (unless already changed)
            if ($rELEVE->getEtat() === $this) {
                $rELEVE->setEtat(null);
            }
        }

        return $this;
    }
}
