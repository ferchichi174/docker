<?php

namespace App\Entity;

use App\Repository\LIGNEABONNERepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LIGNEABONNERepository::class)]
#[ORM\Table(name: '`LIGNE_ABONNE`')]
class LIGNEABONNE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`lgabonne_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $LGABONNE_TARIF = null;

    #[ORM\Column(length: 14)]
    private ?string $CPTEUR_NUM = null;

    #[ORM\Column(length: 3)]
    private ?int $RC_CCPTEUR_CODE = null;

    #[ORM\Column(length: 45)]
    private ?string $amperage = null;

    #[ORM\Column(length: 45)]
    private ?string $coeff = null;


    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIGNE_ABONNE_NBRREMOIS = null;

    #[ORM\OneToOne(inversedBy: 'lIGNEABONNE', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'abonne_id', referencedColumnName: 'abonne_id')]

    private ?ABONNE $ABONNE = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LUSAGE = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIGNE_ABONNE_ETATCOMPTEUR = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $LIGNE_ABONNE_ANCIENINDEX = null;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLGABONNETARIF(): ?string
    {
        return $this->LGABONNE_TARIF;
    }

    public function setLGABONNETARIF(string $LGABONNE_TARIF): self
    {
        $this->LGABONNE_TARIF = $LGABONNE_TARIF;

        return $this;
    }

    public function getCPTEURNUM(): ?string
    {
        return $this->CPTEUR_NUM;
    }

    public function setCPTEURNUM(string $CPTEUR_NUM): self
    {
        $this->CPTEUR_NUM = $CPTEUR_NUM;

        return $this;
    }

    public function getRCCCPTEURCODE(): ?int
    {
        return $this->RC_CCPTEUR_CODE;
    }

    public function setRCCCPTEURCODE(int $RC_CCPTEUR_CODE): self
    {
        $this->RC_CCPTEUR_CODE = $RC_CCPTEUR_CODE;

        return $this;
    }

    public function getAmperage(): ?string
    {
        return $this->amperage;
    }

    public function setAmperage(string $amperage): self
    {
        $this->amperage = $amperage;

        return $this;
    }

    public function getCoeff(): ?string
    {
        return $this->coeff;
    }

    public function setCoeff(string $coeff): self
    {
        $this->coeff = $coeff;

        return $this;
    }




    public function getLIGNEABONNENBRREMOIS(): ?string
    {
        return $this->LIGNE_ABONNE_NBRREMOIS;
    }

    public function setLIGNEABONNENBRREMOIS(?string $LIGNE_ABONNE_NBRREMOIS): self
    {
        $this->LIGNE_ABONNE_NBRREMOIS = $LIGNE_ABONNE_NBRREMOIS;

        return $this;
    }

    public function getABONNE(): ?ABONNE
    {
        return $this->ABONNE;
    }

    public function setABONNE(?ABONNE $ABONNE): self
    {
        $this->ABONNE = $ABONNE;

        return $this;
    }

    public function getLUSAGE(): ?string
    {
        return $this->LUSAGE;
    }

    public function setLUSAGE(?string $LUSAGE): self
    {
        $this->LUSAGE = $LUSAGE;

        return $this;
    }

    public function getLIGNEABONNEETATCOMPTEUR(): ?string
    {
        return $this->LIGNE_ABONNE_ETATCOMPTEUR;
    }

    public function setLIGNEABONNEETATCOMPTEUR(?string $LIGNE_ABONNE_ETATCOMPTEUR): self
    {
        $this->LIGNE_ABONNE_ETATCOMPTEUR = $LIGNE_ABONNE_ETATCOMPTEUR;

        return $this;
    }

    public function getLIGNEABONNEANCIENINDEX(): ?string
    {
        return $this->LIGNE_ABONNE_ANCIENINDEX;
    }

    public function setLIGNEABONNEANCIENINDEX(?string $LIGNE_ABONNE_ANCIENINDEX): self
    {
        $this->LIGNE_ABONNE_ANCIENINDEX = $LIGNE_ABONNE_ANCIENINDEX;

        return $this;
    }








}
