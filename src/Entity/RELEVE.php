<?php

namespace App\Entity;

use App\Repository\RELEVERepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RELEVERepository::class)]
class RELEVE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`releve_id`')]
    private ?int $CPTRLV_ID = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $PQ_DATTRAIT = null;

    #[ORM\Column(length: 2)]
    private ?string $DIST_CODE = null;

    #[ORM\Column]
    private ?int $LOC_CODE = null;

    #[ORM\Column]
    private ?int $PQ_NUM = null;

    #[ORM\Column]
    private ?int $ZONE_CODE = null;

    #[ORM\Column(length: 9)]
    private ?string $CPTRLV_REF = null;

    #[ORM\Column(length: 14)]
    private ?string $CPTRLV_NUMCPTEUR = null;

    #[ORM\Column]
    private ?int $CPTRLV_TARIFCAPTAGE = null;

    #[ORM\Column(length: 3)]
    private ?string $CPTRLV_PUISSCPTEUR = null;

    #[ORM\Column(length: 9)]
    private ?string $CPTRLV_AINDEX = null;

    #[ORM\Column(length: 9)]
    private ?string $CPTRLV_NINDEX = null;

    #[ORM\Column]
    private ?int $CPTRLV_NBRMOIS = null;

    #[ORM\Column]
    private ?int $CPTRLLV_COEFF = null;

    #[ORM\Column]
    private ?int $CPTRLV_CONSMOY = null;


    #[ORM\Column(length: 1)]
    private ?string $ERLV_CODE = null;

    #[ORM\Column(length: 2)]
    private ?string $ERR_CODE = null;

    #[ORM\Column(length: 1)]
    private ?string $CPTRLV_VALIDRELV = null;

    #[ORM\Column(length: 1)]
    private ?string $CPTRLV_VALIDCHEF = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $CPTRLV_DATMAJ = null;

    #[ORM\Column(length: 4)]
    private ?string $CPTRLV_UF_CODE = null;

    #[ORM\Column]
    private ?int $CPTRLV_IDUTILISATEUR = null;

    #[ORM\Column(length: 45)]
    private ?string $EMPCPTR_ACCES = null;

    #[ORM\Column(length: 4)]
    private ?string $EMPCPTR_CODE = null;

    #[ORM\Column(length: 2)]
    private ?string $TYPCPTR_CODE = null;


    #[ORM\Column]
    private ?int $TYPCPTR_NBTARIF = null;

    #[ORM\Column]
    private ?int $TYPCPTR_TARIF1 = null;

    #[ORM\ManyToOne(inversedBy: 'RELEVES')]
    #[ORM\JoinColumn(name: 'observ_id', referencedColumnName: 'observ_id')]
    private ?OBSERVATION $observ_code = null;

    #[ORM\ManyToOne(inversedBy: 'RELEVES')]
    #[ORM\JoinColumn(name: 'type_cpt', referencedColumnName: 'type_cpt_id')]
    private ?TYPECOMPTEUR $typcptr_type = null;

    #[ORM\ManyToOne(inversedBy: 'RELEVES')]
    #[ORM\JoinColumn(name: 'code_etat', referencedColumnName: 'etat_id')]
    private ?ETAT $etat = null;

    #[ORM\ManyToOne(inversedBy: 'RELEVES')]
    #[ORM\JoinColumn(name: 'code_erreur', referencedColumnName: 'erreur_id')]
    private ?ERREUR $ERREUR = null;

    public function getId(): ?int
    {
        return $this->CPTRLV_ID;
    }

    public function getPQDATTRAIT(): ?\DateTimeInterface
    {
        return $this->PQ_DATTRAIT;
    }

    public function setPQDATTRAIT(\DateTimeInterface $PQ_DATTRAIT): self
    {
        $this->PQ_DATTRAIT = $PQ_DATTRAIT;

        return $this;
    }

    public function getDISTCODE(): ?string
    {
        return $this->DIST_CODE;
    }

    public function setDISTCODE(string $DIST_CODE): self
    {
        $this->DIST_CODE = $DIST_CODE;

        return $this;
    }

    public function getLOCCODE(): ?int
    {
        return $this->LOC_CODE;
    }

    public function setLOCCODE(int $LOC_CODE): self
    {
        $this->LOC_CODE = $LOC_CODE;

        return $this;
    }

    public function getPQNUM(): ?int
    {
        return $this->PQ_NUM;
    }

    public function setPQNUM(int $PQ_NUM): self
    {
        $this->PQ_NUM = $PQ_NUM;

        return $this;
    }

    public function getZONECODE(): ?int
    {
        return $this->ZONE_CODE;
    }

    public function setZONECODE(int $ZONE_CODE): self
    {
        $this->ZONE_CODE = $ZONE_CODE;

        return $this;
    }

    public function getCPTRLVREF(): ?string
    {
        return $this->CPTRLV_REF;
    }

    public function setCPTRLVREF(string $CPTRLV_REF): self
    {
        $this->CPTRLV_REF = $CPTRLV_REF;

        return $this;
    }

    public function getCPTRLVNUMCPTEUR(): ?string
    {
        return $this->CPTRLV_NUMCPTEUR;
    }

    public function setCPTRLVNUMCPTEUR(string $CPTRLV_NUMCPTEUR): self
    {
        $this->CPTRLV_NUMCPTEUR = $CPTRLV_NUMCPTEUR;

        return $this;
    }

    public function getCPTRLVTARIFCAPTAGE(): ?int
    {
        return $this->CPTRLV_TARIFCAPTAGE;
    }

    public function setCPTRLVTARIFCAPTAGE(int $CPTRLV_TARIFCAPTAGE): self
    {
        $this->CPTRLV_TARIFCAPTAGE = $CPTRLV_TARIFCAPTAGE;

        return $this;
    }

    public function getCPTRLVPUISSCPTEUR(): ?string
    {
        return $this->CPTRLV_PUISSCPTEUR;
    }

    public function setCPTRLVPUISSCPTEUR(string $CPTRLV_PUISSCPTEUR): self
    {
        $this->CPTRLV_PUISSCPTEUR = $CPTRLV_PUISSCPTEUR;

        return $this;
    }

    public function getCPTRLVAINDEX(): ?string
    {
        return $this->CPTRLV_AINDEX;
    }

    public function setCPTRLVAINDEX(string $CPTRLV_AINDEX): self
    {
        $this->CPTRLV_AINDEX = $CPTRLV_AINDEX;

        return $this;
    }

    public function getCPTRLVNINDEX(): ?string
    {
        return $this->CPTRLV_NINDEX;
    }

    public function setCPTRLVNINDEX(string $CPTRLV_NINDEX): self
    {
        $this->CPTRLV_NINDEX = $CPTRLV_NINDEX;

        return $this;
    }

    public function getCPTRLVNBRMOIS(): ?int
    {
        return $this->CPTRLV_NBRMOIS;
    }

    public function setCPTRLVNBRMOIS(int $CPTRLV_NBRMOIS): self
    {
        $this->CPTRLV_NBRMOIS = $CPTRLV_NBRMOIS;

        return $this;
    }

    public function getCPTRLLVCOEFF(): ?int
    {
        return $this->CPTRLLV_COEFF;
    }

    public function setCPTRLLVCOEFF(int $CPTRLLV_COEFF): self
    {
        $this->CPTRLLV_COEFF = $CPTRLLV_COEFF;

        return $this;
    }

    public function getCPTRLVCONSMOY(): ?int
    {
        return $this->CPTRLV_CONSMOY;
    }

    public function setCPTRLVCONSMOY(int $CPTRLV_CONSMOY): self
    {
        $this->CPTRLV_CONSMOY = $CPTRLV_CONSMOY;

        return $this;
    }


    public function getERLVCODE(): ?string
    {
        return $this->ERLV_CODE;
    }

    public function setERLVCODE(string $ERLV_CODE): self
    {
        $this->ERLV_CODE = $ERLV_CODE;

        return $this;
    }

    public function getERRCODE(): ?string
    {
        return $this->ERR_CODE;
    }

    public function setERRCODE(string $ERR_CODE): self
    {
        $this->ERR_CODE = $ERR_CODE;

        return $this;
    }

    public function getCPTRLVVALIDRELV(): ?string
    {
        return $this->CPTRLV_VALIDRELV;
    }

    public function setCPTRLVVALIDRELV(string $CPTRLV_VALIDRELV): self
    {
        $this->CPTRLV_VALIDRELV = $CPTRLV_VALIDRELV;

        return $this;
    }

    public function getCPTRLVVALIDCHEF(): ?string
    {
        return $this->CPTRLV_VALIDCHEF;
    }

    public function setCPTRLVVALIDCHEF(string $CPTRLV_VALIDCHEF): self
    {
        $this->CPTRLV_VALIDCHEF = $CPTRLV_VALIDCHEF;

        return $this;
    }

    public function getCPTRLVDATMAJ(): ?\DateTimeInterface
    {
        return $this->CPTRLV_DATMAJ;
    }

    public function setCPTRLVDATMAJ(\DateTimeInterface $CPTRLV_DATMAJ): self
    {
        $this->CPTRLV_DATMAJ = $CPTRLV_DATMAJ;

        return $this;
    }

    public function getCPTRLVUFCODE(): ?string
    {
        return $this->CPTRLV_UF_CODE;
    }

    public function setCPTRLVUFCODE(string $CPTRLV_UF_CODE): self
    {
        $this->CPTRLV_UF_CODE = $CPTRLV_UF_CODE;

        return $this;
    }

    public function getCPTRLVIDUTILISATEUR(): ?int
    {
        return $this->CPTRLV_IDUTILISATEUR;
    }

    public function setCPTRLVIDUTILISATEUR(int $CPTRLV_IDUTILISATEUR): self
    {
        $this->CPTRLV_IDUTILISATEUR = $CPTRLV_IDUTILISATEUR;

        return $this;
    }

    public function getEMPCPTRACCES(): ?string
    {
        return $this->EMPCPTR_ACCES;
    }

    public function setEMPCPTRACCES(string $EMPCPTR_ACCES): self
    {
        $this->EMPCPTR_ACCES = $EMPCPTR_ACCES;

        return $this;
    }

    public function getEMPCPTRCODE(): ?string
    {
        return $this->EMPCPTR_CODE;
    }

    public function setEMPCPTRCODE(string $EMPCPTR_CODE): self
    {
        $this->EMPCPTR_CODE = $EMPCPTR_CODE;

        return $this;
    }

    public function getTYPCPTRCODE(): ?string
    {
        return $this->TYPCPTR_CODE;
    }

    public function setTYPCPTRCODE(string $TYPCPTR_CODE): self
    {
        $this->TYPCPTR_CODE = $TYPCPTR_CODE;

        return $this;
    }


    public function getTYPCPTRNBTARIF(): ?int
    {
        return $this->TYPCPTR_NBTARIF;
    }

    public function setTYPCPTRNBTARIF(int $TYPCPTR_NBTARIF): self
    {
        $this->TYPCPTR_NBTARIF = $TYPCPTR_NBTARIF;

        return $this;
    }

    public function getTYPCPTRTARIF1(): ?int
    {
        return $this->TYPCPTR_TARIF1;
    }

    public function setTYPCPTRTARIF1(int $TYPCPTR_TARIF1): self
    {
        $this->TYPCPTR_TARIF1 = $TYPCPTR_TARIF1;

        return $this;
    }

    public function getObservCode(): ?OBSERVATION
    {
        return $this->observ_code;
    }

    public function setObservCode(?OBSERVATION $observ_code): self
    {
        $this->observ_code = $observ_code;

        return $this;
    }

    public function getTypcptrType(): ?TYPECOMPTEUR
    {
        return $this->typcptr_type;
    }

    public function setTypcptrType(?TYPECOMPTEUR $typcptr_type): self
    {
        $this->typcptr_type = $typcptr_type;

        return $this;
    }

    public function getEtat(): ?ETAT
    {
        return $this->etat;
    }

    public function setEtat(?ETAT $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getERREUR(): ?ERREUR
    {
        return $this->ERREUR;
    }

    public function setERREUR(?ERREUR $ERREUR): self
    {
        $this->ERREUR = $ERREUR;

        return $this;
    }
}
