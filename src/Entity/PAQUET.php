<?php

namespace App\Entity;

use App\Repository\PAQUETRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PAQUETRepository::class)]
class PAQUET
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`pq_id`')]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $PQ_DATTRAIT = null;

    #[ORM\Column]
    private ?int $UH_USR_ID = null;


    #[ORM\Column(nullable: true)]
    private ?int $PQ_NUM = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $PQ_DATMAJ = null;

    #[ORM\Column(length: 3, nullable: true)]
    private ?string $PQ_UFCODE = null;

    #[ORM\Column(nullable: true)]
    private ?int $PQ_IDUTILISATEUR = null;

    #[ORM\ManyToOne(inversedBy: 'PAQUET')]
    #[ORM\JoinColumn(name: 'loc_id', referencedColumnName: 'loc_id')]
    private ?LOCALITE $LOC = null;

    #[ORM\ManyToOne(inversedBy: 'pAQUETs')]
    #[ORM\JoinColumn(name: 'dist_id', referencedColumnName: 'dist_id')]
    private ?DIST $DIST = null;

    #[ORM\ManyToOne(inversedBy: 'PAQUETS')]
    #[ORM\JoinColumn(name: 'zone_id', referencedColumnName: 'zone_id')]
    private ?ZONE $ZONE = null;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPQDATTRAIT(): ?\DateTimeInterface
    {
        return $this->PQ_DATTRAIT;
    }

    public function setPQDATTRAIT(\DateTimeInterface $PQ_DATTRAIT): self
    {
        $this->PQ_DATTRAIT = $PQ_DATTRAIT;

        return $this;
    }

    public function getUHUSRID(): ?int
    {
        return $this->UH_USR_ID;
    }

    public function setUHUSRID(int $UH_USR_ID): self
    {
        $this->UH_USR_ID = $UH_USR_ID;

        return $this;
    }


    public function getPQNUM(): ?int
    {
        return $this->PQ_NUM;
    }

    public function setPQNUM(?int $PQ_NUM): self
    {
        $this->PQ_NUM = $PQ_NUM;

        return $this;
    }

    public function getPQDATMAJ(): ?\DateTimeInterface
    {
        return $this->PQ_DATMAJ;
    }

    public function setPQDATMAJ(?\DateTimeInterface $PQ_DATMAJ): self
    {
        $this->PQ_DATMAJ = $PQ_DATMAJ;

        return $this;
    }

    public function getPQUFCODE(): ?string
    {
        return $this->PQ_UFCODE;
    }

    public function setPQUFCODE(?string $PQ_UFCODE): self
    {
        $this->PQ_UFCODE = $PQ_UFCODE;

        return $this;
    }

    public function getPQIDUTILISATEUR(): ?int
    {
        return $this->PQ_IDUTILISATEUR;
    }

    public function setPQIDUTILISATEUR(?int $PQ_IDUTILISATEUR): self
    {
        $this->PQ_IDUTILISATEUR = $PQ_IDUTILISATEUR;

        return $this;
    }

    public function getLOC(): ?LOCALITE
    {
        return $this->LOC;
    }

    public function setLOC(?LOCALITE $LOC): self
    {
        $this->LOC = $LOC;

        return $this;
    }

    public function getDIST(): ?DIST
    {
        return $this->DIST;
    }

    public function setDIST(?DIST $DIST): self
    {
        $this->DIST = $DIST;

        return $this;
    }

    public function getZONE(): ?ZONE
    {
        return $this->ZONE;
    }

    public function setZONE(?ZONE $ZONE): self
    {
        $this->ZONE = $ZONE;

        return $this;
    }



}
