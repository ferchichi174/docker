<?php

namespace App\Entity;

use App\Repository\LOCALITERepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LOCALITERepository::class)]
class LOCALITE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`loc_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $LOC_LIBELLE = null;

    #[ORM\Column(length: 1)]
    private ?string $LOC_TYPERELEVE = null;

    #[ORM\Column]
    private ?int $LOC_VERSION = null;

    #[ORM\Column]
    private ?int $LOC_IDUTILISATEUR = null;

    #[ORM\Column(length: 100)]
    private ?string $LOC_LIBELLE_AR = null;

    #[ORM\Column(length: 10)]
    private ?string $LOC_TEL = null;

    #[ORM\Column(length: 10)]
    private ?string $LOC_TELTECK = null;

    #[ORM\Column(length: 10)]
    private ?string $LOC_TELOLD = null;


    #[ORM\OneToMany(mappedBy: 'LOC', targetEntity: ZONE::class)]
    private Collection $ZONES;

    #[ORM\OneToMany(mappedBy: 'LOC', targetEntity: PAQUET::class)]
    private Collection $PAQUETS;




    #[ORM\OneToMany(mappedBy: 'LOC', targetEntity: ABONNE::class)]
    #[ORM\JoinColumn(name: 'abonne_id', referencedColumnName: 'abonne_id')]
    private Collection $ABONNES;

    #[ORM\ManyToOne(inversedBy: 'LOCALITES')]
    #[ORM\JoinColumn(name: 'dist_id', referencedColumnName: 'dist_id')]
    private ?DIST $DIST_CODE = null;

    #[ORM\OneToMany(mappedBy: 'LOC', targetEntity: ZONE::class)]
    private Collection $zONEs;

    #[ORM\OneToMany(mappedBy: 'LOC', targetEntity: PAQUET::class)]
    private Collection $PAQUET;


    public function __toString(){
        return $this->LOC_LIBELLE;
    }



    public function __construct()
    {
        $this->ZONES = new ArrayCollection();
        $this->PAQUETS = new ArrayCollection();
        $this->ABONNES = new ArrayCollection();
        $this->zONEs = new ArrayCollection();
        $this->PAQUET = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLOCLIBELLE(): ?string
    {
        return $this->LOC_LIBELLE;
    }

    public function setLOCLIBELLE(string $LOC_LIBELLE): self
    {
        $this->LOC_LIBELLE = $LOC_LIBELLE;

        return $this;
    }

    public function getLOCTYPERELEVE(): ?string
    {
        return $this->LOC_TYPERELEVE;
    }

    public function setLOCTYPERELEVE(string $LOC_TYPERELEVE): self
    {
        $this->LOC_TYPERELEVE = $LOC_TYPERELEVE;

        return $this;
    }

    public function getLOCVERSION(): ?int
    {
        return $this->LOC_VERSION;
    }

    public function setLOCVERSION(int $LOC_VERSION): self
    {
        $this->LOC_VERSION = $LOC_VERSION;

        return $this;
    }

    public function getLOCIDUTILISATEUR(): ?int
    {
        return $this->LOC_IDUTILISATEUR;
    }

    public function setLOCIDUTILISATEUR(int $LOC_IDUTILISATEUR): self
    {
        $this->LOC_IDUTILISATEUR = $LOC_IDUTILISATEUR;

        return $this;
    }

    public function getLOCLIBELLEAR(): ?string
    {
        return $this->LOC_LIBELLE_AR;
    }

    public function setLOCLIBELLEAR(string $LOC_LIBELLE_AR): self
    {
        $this->LOC_LIBELLE_AR = $LOC_LIBELLE_AR;

        return $this;
    }

    public function getLOCTEL(): ?string
    {
        return $this->LOC_TEL;
    }

    public function setLOCTEL(string $LOC_TEL): self
    {
        $this->LOC_TEL = $LOC_TEL;

        return $this;
    }

    public function getLOCTELTECK(): ?string
    {
        return $this->LOC_TELTECK;
    }

    public function setLOCTELTECK(string $LOC_TELTECK): self
    {
        $this->LOC_TELTECK = $LOC_TELTECK;

        return $this;
    }

    public function getLOCTELOLD(): ?string
    {
        return $this->LOC_TELOLD;
    }

    public function setLOCTELOLD(string $LOC_TELOLD): self
    {
        $this->LOC_TELOLD = $LOC_TELOLD;

        return $this;
    }



    /**
     * @return Collection<int, ZONE>
     */
    public function getZONES(): Collection
    {
        return $this->ZONES;
    }

    public function addZONE(ZONE $zONE): self
    {
        if (!$this->ZONES->contains($zONE)) {
            $this->ZONES->add($zONE);
            $zONE->setLOC($this);
        }

        return $this;
    }

    public function removeZONE(ZONE $zONE): self
    {
        if ($this->ZONES->removeElement($zONE)) {
            // set the owning side to null (unless already changed)
            if ($zONE->getLOC() === $this) {
                $zONE->setLOC(null);
            }
        }

        return $this;
    }





    /**
     * @return Collection<int, ABONNE>
     */
    public function getABONNES(): Collection
    {
        return $this->ABONNES;
    }

    public function addABONNE(ABONNE $aBONNE): self
    {
        if (!$this->ABONNES->contains($aBONNE)) {
            $this->ABONNES->add($aBONNE);
            $aBONNE->setLOC($this);
        }

        return $this;
    }

    public function removeABONNE(ABONNE $aBONNE): self
    {
        if ($this->ABONNES->removeElement($aBONNE)) {
            // set the owning side to null (unless already changed)
            if ($aBONNE->getLOC() === $this) {
                $aBONNE->setLOC(null);
            }
        }

        return $this;
    }

    public function getDISTCODE(): ?DIST
    {
        return $this->DIST_CODE;
    }

    public function setDISTCODE(?DIST $DIST_CODE): self
    {
        $this->DIST_CODE = $DIST_CODE;

        return $this;
    }

    /**
     * @return Collection<int, PAQUET>
     */
    public function getPAQUET(): Collection
    {
        return $this->PAQUET;
    }

    public function addPAQUET(PAQUET $pAQUET): self
    {
        if (!$this->PAQUET->contains($pAQUET)) {
            $this->PAQUET->add($pAQUET);
            $pAQUET->setLOC($this);
        }

        return $this;
    }

    public function removePAQUET(PAQUET $pAQUET): self
    {
        if ($this->PAQUET->removeElement($pAQUET)) {
            // set the owning side to null (unless already changed)
            if ($pAQUET->getLOC() === $this) {
                $pAQUET->setLOC(null);
            }
        }

        return $this;
    }










}
