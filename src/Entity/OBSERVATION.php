<?php

namespace App\Entity;

use App\Repository\OBSERVATIONRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OBSERVATIONRepository::class)]
class OBSERVATION
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`observ_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2)]
    private ?string $OBSERV_CODE = null;

    #[ORM\Column(length: 100)]
    private ?string $OBSERV_LIBELLEFR = null;

    #[ORM\Column(length: 100)]
    private ?string $OBSERV_LIBELLEAR = null;

    #[ORM\Column(length: 100)]
    private ?string $OBSERV_TRAVEAUXDEMANDE = null;

    #[ORM\Column]
    private ?int $OBSERV_VERSION = null;

    #[ORM\Column]
    private ?int $OBSERV_IDUTILISATEUR = null;

    #[ORM\OneToMany(mappedBy: 'observ_code', targetEntity: RELEVE::class)]
    private Collection $RELEVES;

    public function __construct()
    {
        $this->RELEVES = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOBSERVCODE(): ?string
    {
        return $this->OBSERV_CODE;
    }

    public function setOBSERVCODE(string $OBSERV_CODE): self
    {
        $this->OBSERV_CODE = $OBSERV_CODE;

        return $this;
    }

    public function getOBSERVLIBELLEFR(): ?string
    {
        return $this->OBSERV_LIBELLEFR;
    }

    public function setOBSERVLIBELLEFR(string $OBSERV_LIBELLEFR): self
    {
        $this->OBSERV_LIBELLEFR = $OBSERV_LIBELLEFR;

        return $this;
    }

    public function getOBSERVLIBELLEAR(): ?string
    {
        return $this->OBSERV_LIBELLEAR;
    }

    public function setOBSERVLIBELLEAR(string $OBSERV_LIBELLEAR): self
    {
        $this->OBSERV_LIBELLEAR = $OBSERV_LIBELLEAR;

        return $this;
    }

    public function getOBSERVTRAVEAUXDEMANDE(): ?string
    {
        return $this->OBSERV_TRAVEAUXDEMANDE;
    }

    public function setOBSERVTRAVEAUXDEMANDE(string $OBSERV_TRAVEAUXDEMANDE): self
    {
        $this->OBSERV_TRAVEAUXDEMANDE = $OBSERV_TRAVEAUXDEMANDE;

        return $this;
    }

    public function getOBSERVVERSION(): ?int
    {
        return $this->OBSERV_VERSION;
    }

    public function setOBSERVVERSION(int $OBSERV_VERSION): self
    {
        $this->OBSERV_VERSION = $OBSERV_VERSION;

        return $this;
    }

    public function getOBSERVIDUTILISATEUR(): ?int
    {
        return $this->OBSERV_IDUTILISATEUR;
    }

    public function setOBSERVIDUTILISATEUR(int $OBSERV_IDUTILISATEUR): self
    {
        $this->OBSERV_IDUTILISATEUR = $OBSERV_IDUTILISATEUR;

        return $this;
    }

    /**
     * @return Collection<int, RELEVE>
     */
    public function getRELEVES(): Collection
    {
        return $this->RELEVES;
    }

    public function addRELEVE(RELEVE $rELEVE): self
    {
        if (!$this->RELEVES->contains($rELEVE)) {
            $this->RELEVES->add($rELEVE);
            $rELEVE->setObservCode($this);
        }

        return $this;
    }

    public function removeRELEVE(RELEVE $rELEVE): self
    {
        if ($this->RELEVES->removeElement($rELEVE)) {
            // set the owning side to null (unless already changed)
            if ($rELEVE->getObservCode() === $this) {
                $rELEVE->setObservCode(null);
            }
        }

        return $this;
    }
}
