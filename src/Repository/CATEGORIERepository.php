<?php

namespace App\Repository;

use App\Entity\CATEGORIE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CATEGORIE>
 *
 * @method CATEGORIE|null find($id, $lockMode = null, $lockVersion = null)
 * @method CATEGORIE|null findOneBy(array $criteria, array $orderBy = null)
 * @method CATEGORIE[]    findAll()
 * @method CATEGORIE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CATEGORIERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CATEGORIE::class);
    }

    public function save(CATEGORIE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CATEGORIE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CATEGORIE[] Returns an array of CATEGORIE objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CATEGORIE
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
