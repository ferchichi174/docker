<?php

namespace App\Repository;

use App\Entity\COMPTEUR;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<COMPTEUR>
 *
 * @method COMPTEUR|null find($id, $lockMode = null, $lockVersion = null)
 * @method COMPTEUR|null findOneBy(array $criteria, array $orderBy = null)
 * @method COMPTEUR[]    findAll()
 * @method COMPTEUR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class COMPTEURRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, COMPTEUR::class);
    }

    public function save(COMPTEUR $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(COMPTEUR $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return COMPTEUR[] Returns an array of COMPTEUR objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?COMPTEUR
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
