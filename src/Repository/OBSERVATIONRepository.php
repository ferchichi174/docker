<?php

namespace App\Repository;

use App\Entity\OBSERVATION;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OBSERVATION>
 *
 * @method OBSERVATION|null find($id, $lockMode = null, $lockVersion = null)
 * @method OBSERVATION|null findOneBy(array $criteria, array $orderBy = null)
 * @method OBSERVATION[]    findAll()
 * @method OBSERVATION[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OBSERVATIONRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OBSERVATION::class);
    }

    public function save(OBSERVATION $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(OBSERVATION $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return OBSERVATION[] Returns an array of OBSERVATION objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OBSERVATION
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
