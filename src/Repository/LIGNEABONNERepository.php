<?php

namespace App\Repository;

use App\Entity\LIGNEABONNE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LIGNEABONNE>
 *
 * @method LIGNEABONNE|null find($id, $lockMode = null, $lockVersion = null)
 * @method LIGNEABONNE|null findOneBy(array $criteria, array $orderBy = null)
 * @method LIGNEABONNE[]    findAll()
 * @method LIGNEABONNE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LIGNEABONNERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LIGNEABONNE::class);
    }

    public function save(LIGNEABONNE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LIGNEABONNE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return LIGNEABONNE[] Returns an array of LIGNEABONNE objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?LIGNEABONNE
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
